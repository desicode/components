import resolve from 'rollup-plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';

// `npm run build` -> `production` is true
// `npm run dev` -> `production` is false
const production = process.env.NODE_ENV === 'production';

export default {
  input: 'index.js',
  output: [{
    name: 'components',
    file: 'dist/index.js',
    format: 'cjs', // immediately-invoked function expression - suitable for <script> tags
    sourcemap: !production
  }],
  plugins: [
    resolve(), // tells Rollup how to find date-fns in node_modules
    production && terser() // minify, but only in production
  ]
};
