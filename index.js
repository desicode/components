import { Component } from "preact";
import { toaster } from "./toasterConfig.js";
import { Modal, ModalBody, ModalFooter } from "./modal";
import { html } from 'htm/preact';

class Pagination extends Component {
  onPreviousPageClick() {
    this.props.onChangePageClick(this.props.currentPageNo - 1);
  }

  onNextPageClick() {
    this.props.onChangePageClick(this.props.currentPageNo + 1);
  }

  render(props, {}) {
    if (!Number(props.count)) props.count = 1;
    return html`
      <span>
        <button
          type="button"
          onClick=${this.onPreviousPageClick.bind(this)}
          disabled=${props.currentPageNo === 1}
        >
          ${"<"}
        </button>
        {props.currentPageNo} / {Math.ceil(this.props.count / 10)}
        <button
          type="button"
          onClick=${this.onNextPageClick.bind(this)}
          disabled=${props.currentPageNo === Math.ceil(this.props.count / 10)}
        >
         ${">"}
        </button>
      </span>
    `;
  }
};

const Page = ({ children, filter }) => {
  return html`
    <div class=${"component" + (filter ? " with-filter" : "")}>${children}</div>
  `;
};

const Filter = ({ children, title = "Filter" }) => {
  return html`
    <section class="filter">
      <header class="box">
        <h6>{title}</h6>
      </header>
      <section>${children}</section>
    </section>
  `;
};

const Header = ({ title, children }) => {
  return html`
    <header class="box">
      <h6>${title}</h6>
      <section>${children}</section>
    </header>
  `;
};

const Content = ({ children }) => {
  return html`<section class="content">${children}</section>`;
};

const Card = ({ children }) => {
  return html`<div class="card">${children}</div>`;
};

/**
 * On-screen toast message.
 * @param {string} text - Message text.
 * @param {string} [type] - Toast.TYPE_*
 * @param {number} [timeout] - Toast.TIME_*
 * @constructor
 */
const Toast = function(
  text = `No text!`,
  type = Toast.TYPE_INFO,
  timeout = Toast.TIME_LONG
) {
  let el1 = document.createElement("div"),
    el2 = document.createElement("div");

  el1.className = "toast";
  el2.className = `body ${type}`;
  el1.appendChild(el2);
  el2.innerHTML = `${text}`;

  this.element = el1;
  this.position = 0;

  toaster.push(this, timeout);
}

Toast.TYPE_INFO = "info";
Toast.TYPE_MESSAGE = "message";
Toast.TYPE_WARNING = "warning";
Toast.TYPE_ERROR = "error";
Toast.TYPE_DONE = "done";

Toast.TIME_SHORT = 2000;
Toast.TIME_NORMAL = 4000;
Toast.TIME_LONG = 8000;

let options = {
  deleteDelay: 300,
  topOrigin: 0
};

/**
 * Allows you to configure Toasts options during the application setup.
 * @param newOptions
 */
const configureToasts = (newOptions = {}) => {
  Object.assign(options, newOptions);
}

/**
 * Delete all toast currently displayed.
 */
const deleteAllToasts = () => {
  return toaster.removeAll();
}

/**
 * Attaches toast to DOM and returns the height of the element.
 */
Toast.prototype.attach = function(position) {
  this.position = position;
  this.updateVisualPosition();
  document.body.appendChild(this.element);
  requestAnimationFrame(() => {
    this.element.classList.add("displayed");
  });

  return this.element.offsetHeight;
};

/**
 * Seek the toast message by Y coordinate.
 * @param delta
 */
Toast.prototype.seek = function(delta) {
  this.position += delta;
  this.updateVisualPosition();
};

/**
 * @private
 */
Toast.prototype.updateVisualPosition = function() {
  requestAnimationFrame(() => {
    this.element.style.bottom = -options.topOrigin + this.position + "px";
  });
};

/**
 * Removes toast from DOM.
 */
Toast.prototype.detach = function() {
  let self = this;

  if (!this.element.parentNode) return;

  requestAnimationFrame(() => {
    this.element.classList.remove("displayed");
    this.element.classList.add("deleted");
  });
  setTimeout(() => {
    requestAnimationFrame(() => {
      if (!self.element || !self.element.parentNode) return;
      self.element.parentNode.removeChild(self.element);
    });
  }, options.deleteDelay);
};

Toast.prototype.delete = function() {
  toaster.remove(this);
};

/**
 * @param date - Date to be formatted, need not be JS date, ISO string would be fine as well
 * @param isTimeRequired - If set to true, will also return the time of the day component
 */
const formatDateTime = (date, isTimeRequired) => {
  const options = {
    year: "numeric",
    month: "numeric",
    day: "numeric",
    timeZone: "Asia/Kolkata"
  };
  let formattedDate = "";
  if (isTimeRequired) {
    options.hour = "numeric";
    options.minute = "numeric";
    options.second = "numeric";
  }
  if (date) {
    formattedDate = new Intl.DateTimeFormat("en-IN", options).format(
      new Date(date)
    );
  }
  return formattedDate;
}

const getRelativeTime = (date, relativeTime) => {
  // console.log(`input date - ${date}, relativeTime - ${relativeTime}`)
  // console.log(new Date(date))
  let milliseconds = 0
  if (relativeTime) {
    milliseconds = new Date().getTime() - new Date(date).getTime()
  } else {
    milliseconds = new Date(date).getTime()
  }

  const numberEnding = (number) => {
    return (number > 1) ? 's' : ''
  }

  let temp = Math.floor(milliseconds / 1000)
  const years = Math.floor(temp / 31536000)
  if (years) {
    return years + ' year' + numberEnding(years)
  }
  // TODO: Months! Maybe weeks?
  const days = Math.floor((temp %= 31536000) / 86400)
  if (days) {
    return days + ' day' + numberEnding(days)
  }
  const hours = Math.floor((temp %= 86400) / 3600)
  if (hours) {
    return hours + ' hour' + numberEnding(hours)
  }
  const minutes = Math.floor((temp %= 3600) / 60)
  if (minutes) {
    return minutes + ' minute' + numberEnding(minutes)
  }
  const seconds = temp % 60
  if (seconds) {
    return seconds + ' second' + numberEnding(seconds)
  }
  // 'just now' //or other string you like;
  return '0'
}

const formatAmount = (amount) => {
  if (amount) {
    return new Intl.NumberFormat("en-IN", {
      style: "currency",
      currency: "INR"
    }).format(amount);
  }
  return "-";
}

const startLoader = () => {
  let element = document.getElementById("loader-bg");
  element.style.display = "flex";
}

const stopLoader = () => {
  let element = document.getElementById("loader-bg");
  element.style.display = "none";
}

export {
  Modal,
  ModalBody,
  ModalFooter,
  Pagination,
  Page,
  Filter,
  Header,
  Content,
  Card,
  formatDateTime,
  formatAmount,
  startLoader,
  stopLoader,
  configureToasts,
  deleteAllToasts,
  Toast
};
